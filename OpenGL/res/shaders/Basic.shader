#shader vertex
#version 330 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 color;
varying vec4 f_color;


void main()
{
   gl_Position = position;
   f_color = vec4(color.r, color.g, color.b, 1.0);
};

#shader fragment
#version 330 core

varying vec4 f_color;

void main()
{
	gl_FragColor = f_color;	
};