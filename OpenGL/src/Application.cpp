#include <GL/glew.h>
#include <GLFW/glfw3.h> 
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

struct ShaderProgramSource
{
    std::string VertexSource;
    std::string FragmentSource;
};

//parsing shaders from Basic.shader
static ShaderProgramSource parseShader(const std::string& filepath) 
{
    std::ifstream stream(filepath);

    enum class ShaderType 
    {
        NONE = -1, VERTEX = 0, FRAGMENT = 1
    };

    std::string line;
    std::stringstream ss[2];

    ShaderType type = ShaderType::NONE;

    while (getline(stream, line)) 
    { 
        if(line.find("#shader") != std::string::npos)
        {
            if (line.find("vertex") != std::string::npos)
                type = ShaderType::VERTEX;
            else if (line.find("fragment") != std::string::npos)
                type = ShaderType::FRAGMENT;
        }
        else
        {
            ss[(int)type] << line << '\n';
        }
    }
    return {ss[0].str(), ss[1].str()};
}

static unsigned int compileShader(unsigned int type, const std::string& source) {
    unsigned int id = glCreateShader(type);
    const char* src = source.c_str();
    glShaderSource(id, 1, &src, nullptr);
    glCompileShader(id);

    int result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE) {
        int length;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
        char* message = (char*)alloca(length * sizeof(char));
        glGetShaderInfoLog(id, length, &length, message);
        std::cout << "Failed To Compile" <<
            (type == GL_VERTEX_SHADER ? "shader" : "fragment") << "shader!" << std::endl;
        std::cout << message << std::endl;
        glDeleteShader(id);
        return 0;
    }
    return id;
}

static unsigned int CreateShader(const std::string& vertexShader, const std::string& fragmentShader) {

    unsigned int program = glCreateProgram();
    unsigned int vs = compileShader(GL_VERTEX_SHADER, vertexShader);
    unsigned int fs = compileShader(GL_FRAGMENT_SHADER, fragmentShader);

    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    glValidateProgram(program);

    glDeleteShader(vs);
    glDeleteShader(fs);

    return program;
}

void get_resolution()
{
    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

    int x = mode->width;
    int y = mode->height;

    std::cout << "The resolution of this screen is: " << x << "*" << y << std::endl;
}

int main(void)
{
    GLFWwindow* window;
    /* Initialize the library */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(315, 420, "Flag Of Nepal", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    //get resolution of monitor
    get_resolution();

    if (glewInit() != GLEW_OK)
        std::cout << "ERROR!" << std::endl;
    
    float positions[] = 
    {
        //flag border 12PTS
        //bottom tirangle outer
        -0.83589f, -0.95089f, 0, 0.219f, 0.576f,
        -0.83589f,  0.58482f, 0, 0.219f, 0.576f,
         0.9282f , -0.95089f, 0, 0.219f, 0.576f,
         //top triangle outer   
        -0.83589f, -0.004642f, 0, 0.219f, 0.576f,
        -0.83589f,  1.0f     , 0, 0.219f, 0.576f,
         1.0f    ,  0.0004642f, 0, 0.219f, 0.576f,         
         //bottom triangle inner
        -0.76923f, -0.89285f, 1, 0.078f, 0.235f,
        -0.76923f,  0.44642f, 1, 0.078f, 0.235f,
         0.76932f, -0.89285f, 1, 0.078f, 0.235f,         
         //top triangle inner
        -0.76923f, 0.89285f, 1, 0.078f, 0.235f,
         0.76923f, 0.05357f, 1, 0.078f, 0.235f,
        -0.76923f, 0.05357f, 1, 0.078f, 0.235f,         
        
        //sun vertices  26PTS
        -0.38461f, -0.41964f, 1, 1, 1,  //U1 -> centre
        -0.38461f, -0.13392f, 1, 1, 1,  //W2
        -0.44102f, -0.24553f, 1, 1, 1,  //W1
        -0.54871f, -0.16964f, 1, 1, 1,  //G3
        -0.53333f, -0.29017f, 1, 1, 1,  //A2
        -0.67179f, -0.27678f, 1, 1, 1,  //I3
        -0.58461f, -0.375f,   1, 1, 1,  //C2
        -0.71282f, -0.41964f, 1, 1, 1,  //Z2       
        -0.58461f, -0.46428f, 1, 1, 1,  //V2
        -0.67179f, -0.55803f, 1, 1, 1,  //E3
        -0.53333f, -0.54464f, 1, 1, 1,  //T2
        -0.54871f, -0.66517f, 1, 1, 1,  //C3
        -0.44102f, -0.59375f, 1, 1, 1,  //R2
        -0.38461f, -0.70535f, 1, 1, 1,  //A3
        -0.32820f, -0.59375f, 1, 1, 1,  //Q2
        -0.22051f, -0.66964f, 1, 1, 1,  //H3
        -0.23589f, -0.54910f, 1, 1, 1,  //O2
        -0.10256f, -0.5625f,  1, 1, 1,  //J3
        -0.18461f, -0.46875f, 1, 1, 1,  //M2
        -0.05641f, -0.41964f, 1, 1, 1,  //B3
        -0.18461f, -0.375f,   1, 1, 1,  //L2
        -0.09743f, -0.27678f, 1, 1, 1,  //F3
        -0.23589f, -0.29017f, 1, 1, 1,  //I2
        -0.22051f, -0.17410f, 1, 1, 1,  //D3
        -0.32820f, -0.24553f, 1, 1, 1,  //G2  
        -0.38461f, -0.13392f, 1, 1, 1,  //W2    

        //top moon crown 20PTS            
        -0.38461f, 0.24553f, 1, 1, 1,   //U -> centre
        -0.58461f, 0.24533f, 1, 1, 1,   //S
        -0.52820f, 0.25892f, 1, 1, 1,   //O3
        -0.56923f, 0.30357f, 1, 1, 1,   //B4
        -0.51282f, 0.30357f, 1, 1, 1,   //P3
        -0.53333f, 0.35714f, 1, 1, 1,   //C4
        -0.48205f, 0.33482f, 1, 1, 1,   //Q3
        -0.48205f, 0.39285f, 1, 1, 1,   //D4
        -0.43589f, 0.36160f, 1, 1, 1,   //R3
        -0.41538f, 0.41517f, 1, 1, 1,   //E4
        -0.38461f, 0.37053f, 1, 1, 1,   //M
        -0.35384f, 0.41517f, 1, 1, 1,   //I4
        -0.33333f, 0.36160f, 1, 1, 1,   //S3
        -0.28717f, 0.39285f, 1, 1, 1,   //H4
        -0.28717f, 0.33482f, 1, 1, 1,   //U3
        -0.23589f, 0.35714f, 1, 1, 1,   //G4
        -0.25641f, 0.29910f, 1, 1, 1,   //V3
        -0.2f    , 0.30357f, 1, 1, 1,   //F4
        -0.24102f, 0.25892f, 1, 1, 1,   //Z3
        -0.18461f, 0.24553f, 1, 1, 1,    //T    

        //crescent moon
        -0.6932f , 0.37053f, 1, 1, 1,
        -0.68205f, 0.35714f, 1, 1, 1,
        -0.68717f, 0.33928f, 1, 1, 1,
        -0.66667f, 0.33035f, 1, 1, 1,
        -0.68205f, 0.30357f, 1, 1, 1,
        -0.64615f, 0.30357f, 1, 1, 1,
        -0.66667f, 0.26785f, 1, 1, 1,
        -0.62051f, 0.27678f, 1, 1, 1,
        -0.64615f, 0.22767f, 1, 1, 1,
        -0.58461f, 0.24553f, 1, 1, 1,
        -0.6f    , 0.17857f, 1, 1, 1,
        -0.46667f, 0.24553f, 1, 1, 1,
        -0.55384f, 0.14732f, 1, 1, 1,
        -0.38461f, 0.24553f, 1, 1, 1,
        -0.48205f, 0.11607f, 1, 1, 1,
        -0.30256f, 0.24553f, 1, 1, 1,
        -0.38461f, 0.10267f, 1, 1, 1,
        -0.18461f, 0.24553f, 1, 1, 1,
        -0.28717f, 0.11607f, 1, 1, 1,
        -0.22564f, 0.14285f, 1, 1, 1,
        -0.18461f, 0.24553f, 1, 1, 1,
        -0.17948f, 0.17410f, 1, 1, 1,
        -0.15384f, 0.26785f, 1, 1, 1,
        -0.14358f, 0.20535f, 1, 1, 1,
        -0.13333f, 0.29017f, 1, 1, 1, 
        -0.10769f, 0.25f   , 1, 1, 1,
        -0.10769f, 0.31696f, 1, 1, 1,
        -0.09230f, 0.28571f, 1, 1, 1,
        -0.07692f, 0.34821f, 1, 1, 1,
        -0.08205f, 0.32142f, 1, 1, 1,
        -0.07692f, 0.37053f, 1, 1, 1,
        -0.07692f, 0.34821f, 1, 1, 1 
    };

    unsigned int buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER,sizeof(positions), positions, GL_STATIC_DRAW);
 
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, sizeof(float) * 5, (void*)(sizeof(float) * 2));
      

    ShaderProgramSource source = parseShader("res/shaders/Basic.shader");
    unsigned int shader = CreateShader(source.VertexSource, source.FragmentSource);
    glUseProgram(shader);    
   
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Render here */
        glClearColor(0, 0, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT);

        glDrawArrays(GL_TRIANGLES, 0, 12);
            
        glDrawArrays(GL_TRIANGLE_FAN, 12, 26);

        glDrawArrays(GL_TRIANGLE_FAN, 38, 20);

        glDrawArrays(GL_TRIANGLE_STRIP, 58, 32);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    //Clear shaders
   glDeleteProgram(shader);

    glfwTerminate();
    return 0;
}

